#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include "ppm.h"

/*static inline*/ struct image* img_new (size_t width, size_t height) {
    struct image* foo = malloc(sizeof(struct image));
    foo->width = width;
    foo->height = height;
    foo->maxval = 255;
    foo->raster = calloc(width*height, sizeof(uint8_t));
    return foo;
}

/*static inline*/ void img_free (struct image* img) {
    free(img->raster);
    free(img);
}

#ifdef CHECK_RANGES
/*static inline*/ void img_bounds_check (struct image* i, size_t x, size_t y) {
    if (!(x < i->width && y < i->height)) {
        fprintf(stderr, "ERROR: Requested pixel (%zu, %zu) outside image (of dimensions (%zu, %zu)). Coordinates are 0-based with the origin at the lower-left.\n", x, y, i->width, i->height);
        exit(0);
    }
}
#endif

/*static inline*/ uint8_t img_get_pixel (struct image* i, size_t x, size_t y) {
#ifdef CHECK_RANGES
    img_bounds_check(i, x, y);
#endif
    return i->raster[IMG_GET_OFFSET(i, x, y)];
}

/*static inline*/ void img_set_pixel (struct image* i, size_t x, size_t y, uint8_t val) {
#ifdef CHECK_RANGES
    img_bounds_check(i, x, y);
#endif
    i->raster[IMG_GET_OFFSET(i, x, y)] = val;
}

/*static inline*/ void img_set_pixel_from_unit_square_coords (struct image* i, double x, double y, uint8_t val) {
#ifdef CHECK_RANGES
    if (!(x <= 1 && y <= 1 && x >= 0 && y >= 0)) {
        printf("ERROR: Coordinate (%f, %f) outside of unit square.\n", x, y);
        exit(0);
    }
#endif
    size_t newX = round(x * (i->width-1));
    size_t newY = round(y * (i->height-1));
    img_set_pixel(i, newX, newY, val);
}

/*static*/ void img_output_ppm (struct image* img, struct color (*palette)(uint8_t n), const char* filename) {
    FILE* f = fopen(filename, "w");
    if (f) {
        fprintf(f, "P6\n%zu\n%zu\n%d\n", img->width, img->height, img->maxval);
        char intensity;
        for (size_t y = img->height-1; 1; y--) {
            for (size_t x = 0; x < img->width; x++) {
                intensity = img_get_pixel(img, x, y);
                struct color c = palette(intensity);
                fprintf(f, "%c%c%c", c.r, c.g, c.b);
            }
            if (y == 0)
                break;
        }
        if (ferror(f)) {
            printf("An error occured while writing file \"%s\".\n", filename);
            exit(0);
        }
        if (fclose(f)) {
            printf("Failed to close file \"%s\" following writing.\n", filename);
            exit(0);
        }
    } else {
        printf("Failed to open file \"%s\" for writing.\n", filename);
        exit(0);
    }
}

/*static*/ void img_output_png (struct image* img, struct color (*palette)(uint8_t n), const char* filename) {
  //Write ppm
  img_output_ppm(img, palette, filename);
  //Convert to png with ImageMagick
  char command[256];
  sprintf(command, "convert ppm:%s png:%s", filename, filename);
  system(command); //BUG:WONTFIX:Just a horrible security bug!
}

/*static inline*/ void img_draw_y_equal_x (struct image* i) {
    for (size_t x = 0; x < i->width; x++)
        img_set_pixel(i, x, (i->height/(double)i->width)*x, 1);
}
/*end .c*/
