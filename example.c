#define CHECK_RANGES
#include <ppmSingleHeader.h>
#include <math.h>
#ifndef M_PI
#define M_PI 3.14159
#endif

struct color palette (uint8_t n) {
  struct color c;
  double brightness = ((n>>4)*16)/256.0;
  c.r = (n&0x0F)*16*brightness;
  c.g = 0;
  c.b = c.r;
  return c;
}

int main (void) {
  int w = 640, h = 480;
  struct image* img = img_new(w,h);
  for (int y = 0; y < h; ++y) {
    for (int x = 0; x < w; ++x) {
      int overflowCount = 2;
      int checkerIntensity = ((sin(x/8.0) + cos(y/8.0) + 2)/4.0)*16*overflowCount;
      //(xx,yy) is (x,y) but with the origin at (w/2, h/2)
      int xx = x-w/2;
      int yy = y-h/2;
      double dist = sqrt(xx*xx+yy*yy);
      double maxDist = sqrt((w/2)*(w/2) + (h/2)*(h/2));
      int circleTint = (dist/maxDist)*16*overflowCount;
      img_set_pixel(img, x, y, checkerIntensity + (circleTint<<4));
    }
  }
  img_output_ppm(img, palette, "test.ppm");
  img_output_png(img, palette, "test.png");
  img_free(img);
  return 0;
}
