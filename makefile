NAME=ppm
CC=gcc
FLAGS=-Wall -Wextra -Werror -std=c99 -pedantic -g -Iinclude
LINK=-lm
CFILES=$(NAME).c
HFILES=$(NAME).h
FULLNAME=lib$(NAME).so.1.0
MAJORNAME=lib$(NAME).so.1
SMALLNAME=lib$(NAME).so
STATICNAME=lib$(NAME).a
LIBDIR=/usr/lib
INCLUDEDIR=/usr/include

all : install example
	@echo "Complete"

install : static dynamic singleheader
	@cp $(NAME).h release/
	@sudo rm -rf /usr/local/include/$(NAME)
	@sudo cp release/$(NAME).h $(INCLUDEDIR)/$(NAME).h
	@sudo cp release/$(NAME)SingleHeader.h $(INCLUDEDIR)/$(NAME)SingleHeader.h
	@sudo cp release/$(FULLNAME) $(LIBDIR)/
	@sudo ln -sf $(LIBDIR)/$(FULLNAME) $(LIBDIR)/$(SMALLNAME)
	@sudo ln -sf $(LIBDIR)/$(FULLNAME) $(LIBDIR)/$(MAJORNAME)
	@sudo ldconfig

static : $(CFILES) $(HFILES) clean
	@mkdir -p release
	@$(CC) $(FLAGS) -c $(CFILES) $(LINK)
	@ar rcs $(STATICNAME) *.o
	@mv $(STATICNAME) release/
	@rm -f *.o

dynamic : $(CFILES) $(HFILES) clean
	@mkdir -p release
	@$(CC) $(FLAGS) -fPIC -c $(CFILES) $(LINK)
	@$(CC) --shared -Wl,-soname,$(MAJORNAME) -o $(FULLNAME) *.o
	@mv $(FULLNAME) release/
	@ln -sf release/$(FULLNAME) release/$(SMALLNAME)
	@ln -sf release/$(FULLNAME) release/$(MAJORNAME)
	@rm -f *.o

singleheader : $(CFILES) $(HFILES) clean
	@sed 's:/\*static inline\*/:static inline:g; s:/\*static\*/:static:g; s:/\*end .h\*/:#ifndef $(NAME)_C\n#define $(NAME)_C:g; s:/\*end .c\*/:#endif:g' $(NAME).h $(NAME).c > release/$(NAME)SingleHeader.h

example : install example.c
	@gcc example.c -o example -lm

clean :
	@rm -rf release
