//Origin is at the lower-left.
//Raster is row-major left-to-right top-to-bottom despite the origin being in the lower-left.
//#define CHECK_RANGES to make img_get/set_pixel print an error to stderr and exit(0) on invalid coords.

#ifndef PPM_H
#define PPM_H
#include <stdlib.h>
#include <stdint.h>

struct image {
    size_t width, height;
    uint8_t maxval;
    uint8_t* raster;
};

struct color {
    uint8_t r,g,b;
};

//WARNING: Free returned pointer with img_free().
/*static inline*/ struct image* img_new        (size_t width, size_t height);
/*static inline*/ void          img_free       (struct image* img);
/*static inline*/ uint8_t       img_get_pixel  (struct image* i, size_t x, size_t y);
/*static inline*/ void          img_set_pixel  (struct image* i, size_t x, size_t y, uint8_t val);
//The palette function turns uint8_ts into RGB triplets.
/*static*/        void          img_output_ppm (struct image* img, struct color (*palette)(uint8_t n), const char* filename);
/*static*/        void          img_output_png (struct image* img, struct color (*palette)(uint8_t n), const char* filename);
/*static inline*/ void          img_set_pixel_from_unit_square_coords (struct image* i, double x, double y, uint8_t val);
#define IMG_GET_OFFSET(I,X,Y) (((I)->width * (((I)->height-1)-(Y))) + (X))

#endif
/*end .h*/
